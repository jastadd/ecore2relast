# PNML Specification Disclaimer

http://www.pnml.org/disclaimer.php

© Pnml.org 2009-2015.

We are providing the meta-models of PNML specification with the following disclaimer:

- Give credit to Pnml.org for these meta-models.
- In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of these meta-models.
- We are the maintainers of these meta-models, therefore we may at any moment update them.
- Please, give us feedback about your use of these meta-models, including bugs and further modifications of your own and the features of the tool that relies upon them. We will release that information about your tool on this website, upon your agreement.
