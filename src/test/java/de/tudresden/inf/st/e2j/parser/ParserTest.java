package de.tudresden.inf.st.e2j.parser;

import de.tudresden.inf.st.e2j.AbstractTest;
import de.tudresden.inf.st.e2j.EcoreFileProvider;
import de.tudresden.inf.st.e2j.jastadd.model.EObject;
import de.tudresden.inf.st.e2j.jastadd.model.EPackage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class ParserTest extends AbstractTest {


  private static Logger logger = LogManager.getLogger(ParserTest.class);

  @BeforeAll
  static void setUpDirectories() throws IOException {
    deleteFolder("src/gen/resources/ecore/zoo/");
    deleteFolder("src/gen/resources/ecore/");

    // if the required directories do not exist, create them
    Files.createDirectories(Paths.get("src/gen/resources/ecore/zoo"));
  }

  @ParameterizedTest
  @ArgumentsSource(EcoreFileProvider.class)
  void parseEcore(String fileName) {

    logger.info("Parsing file src/test/resources/ecore/zoo/{}", fileName);

    EObject ePackage = null;

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream("/ecore/zoo/" + fileName)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + fileName + "'");
      ePackage = parser.parse(stream).get(0);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackage);

    Assertions.assertTrue(ePackage instanceof EPackage);

    EPackage p = (EPackage) ePackage;

    StringBuilder b = new StringBuilder();

    logger.info("writing {} packages", p.getPackageList().size());
    p.writeXMI(b, "ISO-8859-1");

    try (PrintWriter out = new PrintWriter("src/gen/resources/ecore/zoo/" + fileName, StandardCharsets.ISO_8859_1.name())) {
      out.print(b.toString());
    } catch (FileNotFoundException | UnsupportedEncodingException e) {
      Assertions.fail("unable to write output file", e);
    }

  }

  @Test
  void parseStateMachine() {

    EObject ePackage = null;

    final String fileName = "/ecore/statemachine.ecore";

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream(fileName)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + fileName + "'");
      ePackage = parser.parse(stream).get(0);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackage);

    Assertions.assertTrue(ePackage instanceof EPackage);

    EPackage p = (EPackage) ePackage;

    StringBuilder b = new StringBuilder();

    p.writeXMI(b);

    try (PrintWriter out = new PrintWriter("src/gen/resources/ecore/statemachine.ecore")) {
      out.print(b.toString());
    } catch (FileNotFoundException e) {
      Assertions.fail("unable to write output file", e);
    }

    System.out.println(b.toString());
  }

  @Test
  void parseBigraph() {

    EObject ePackage = null;

    final String fileName = "/ecore/bigraph.ecore";

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream(fileName)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + fileName + "'");
      ePackage = parser.parse(stream).get(0);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackage);

    Assertions.assertTrue(ePackage instanceof EPackage);

    EPackage p = (EPackage) ePackage;

    StringBuilder b = new StringBuilder();

    p.writeXMI(b);

    try (PrintWriter out = new PrintWriter("src/gen/resources/ecore/bigraph.ecore")) {
      out.print(b.toString());
    } catch (FileNotFoundException e) {
      Assertions.fail("unable to write output file", e);
    }

    System.out.println(b.toString());
  }

  @Test
  void parseEcore() {

    EObject ePackage = null;

    final String fileName = "/ecore/ecore.ecore";

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream(fileName)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + fileName + "'");
      ePackage = parser.parse(stream).get(0);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackage);

    Assertions.assertTrue(ePackage instanceof EPackage);

    EPackage p = (EPackage) ePackage;

    StringBuilder b = new StringBuilder();

    p.writeXMI(b);

    try (PrintWriter out = new PrintWriter("src/gen/resources/ecore/ecore.ecore")) {
      out.print(b.toString());
    } catch (FileNotFoundException e) {
      Assertions.fail("unable to write output file", e);
    }

    System.out.println(b.toString());
  }

}
