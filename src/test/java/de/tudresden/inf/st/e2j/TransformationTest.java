package de.tudresden.inf.st.e2j;

import de.tudresden.inf.st.e2j.jastadd.model.EObject;
import de.tudresden.inf.st.e2j.jastadd.model.EPackage;
import de.tudresden.inf.st.e2j.jastadd.model.Grammar;
import de.tudresden.inf.st.e2j.parser.EcoreParser;
import de.tudresden.inf.st.e2j.parser.XMIParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class TransformationTest extends AbstractTest {

  private static final Logger logger = LogManager.getLogger(TransformationTest.class);

  @BeforeAll
  static void setUpDirectories() throws IOException {
    deleteFolder("src/gen/resources/grammar/zoo/");
    deleteFolder("src/gen/resources/grammar/");

    // if the required directories do not exist, create them
    Files.createDirectories(Paths.get("src/gen/resources/grammar/zoo"));
  }

  @ParameterizedTest
  @ArgumentsSource(EcoreFileProvider.class)
  void transformEcore(String fileName) {

    logger.info("transforming file src/test/resources/ecore/zoo/{}", fileName);

    List<EObject> ePackages = null;

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream("/ecore/zoo/" + fileName)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + fileName + "'");
      ePackages = parser.parse(stream);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackages);

    Assertions.assertTrue(ePackages.size() > 0, "there should be at least one ePackage in an ecore model");

    StringBuilder b = new StringBuilder();

    for (EObject eObject : ePackages) {
      Assertions.assertTrue(eObject instanceof EPackage);

      Grammar grammar = ((EPackage) eObject).getGrammar();
      grammar.print(b);
      b.append("\n\n");
    }

    fileName = fileName.replaceFirst("\\.ecore$", ".relast");
    logger.info("writing grammar to {}", fileName);

    try (PrintWriter out = new PrintWriter("src/gen/resources/grammar/zoo/" + fileName, StandardCharsets.UTF_8.name())) {
      out.print(b.toString());
    } catch (FileNotFoundException | UnsupportedEncodingException e) {
      Assertions.fail("unable to write output file", e);
    }

  }

  /**
   * transforms an ecore model into a relAST grammar
   *
   * @param sourceFile the source file (relative to src/test/resources)
   * @param targetFile the target file, relative to the project base directory
   */
  private void transformFile(String sourceFile, String targetFile) {

    EObject ePackage = null;

    EcoreParser parser = new EcoreParser();

    try (InputStream stream = this.getClass().getResourceAsStream(sourceFile)) {
      Assertions.assertNotNull(stream, "unable to load resource '" + sourceFile + "'");
      ePackage = parser.parse(stream).get(0);
    } catch (IOException | XMIParseException e) {
      Assertions.fail(e);
    }

    Assertions.assertNotNull(ePackage);

    Assertions.assertTrue(ePackage instanceof EPackage);

    Grammar g = ((EPackage) ePackage).getGrammar();

    StringBuilder b = new StringBuilder();

    g.print(b);

    Path sourcePath = Paths.get("src/test/resources/" + sourceFile);
    if (Files.notExists(sourcePath)) {
      Assertions.fail("the source file " + sourcePath.toAbsolutePath().getFileName() + " does not exist.");
    }

    Main.transformFile("src/test/resources/" + sourceFile, targetFile);
  }

  @Test
  void transformStateMachine() {
    transformFile("/ecore/statemachine.ecore", "src/gen/resources/grammar/statemachine.relast");
  }

  @Test
  void transformCROM() {
    transformFile("/ecore/crom.ecore", "src/gen/resources/grammar/crom.relast");
  }

  @Test
  void transformPNML() {
    transformFile("/ecore/pnml/pnmlcoremodel.ecore", "src/gen/resources/grammar/pnmlcoremodel.relast");
    transformFile("/ecore/pnml/placeTransition.ecore", "src/gen/resources/grammar/placeTransition.relast");
  }

  @Test
  void transformTrain() {
    transformFile("/ecore/Train.ecore", "src/gen/resources/grammar/Train.relast");
  }

  @Test
  void transformBigraph() {
    transformFile("/ecore/bigraph.ecore", "src/gen/resources/grammar/bigraph.relast");
  }

  @Test
  void transformEcore() {
    transformFile("/ecore/ecore.ecore", "src/gen/resources/grammar/ecore.relast");
  }

}
