package de.tudresden.inf.st.e2j;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class EcoreFileProvider implements ArgumentsProvider {

  @Override
  public Stream<? extends Arguments> provideArguments(ExtensionContext context) {

    try (InputStream stream = this.getClass().getResourceAsStream("/ecore/zoo")) {
      return new BufferedReader(new InputStreamReader(stream))
          .lines()
          .filter(fileName -> fileName.endsWith(".ecore"))
          .map(Arguments::of);
    } catch (IOException e) {
      Assertions.fail(e);
    }

    // no stream if something fails
    return Stream.empty();
  }
}
