package de.tudresden.inf.st.e2j.parser;

import de.tudresden.inf.st.e2j.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.util.*;

public class EcoreParser {

  private static final String XSI_NS = "http://www.w3.org/2001/XMLSchema-instance";
  private static final QName XSI_TYPE = new QName(XSI_NS, "type");

  private static Logger logger = LogManager.getLogger(EcoreParser.class);
  private final XMLInputFactory factory = XMLInputFactory.newInstance();

  public List<EObject> parse(InputStream stream) throws XMIParseException {

    try {
      final XMLEventReader reader = factory.createXMLEventReader(stream);

      // TODO there may be one or more elements in the root, if so, the top level element is called XMI (?)

      while (reader.hasNext()) {
        final XMLEvent event = reader.peek();

        if (event.isStartDocument()) {
          reader.nextEvent();
        } else if (event.isStartElement()) {
          StartElement root = event.asStartElement();

          switch (root.getName().getLocalPart()) {
            case "XMI":
              return parseXMI(reader, Collections.EMPTY_MAP);
            case "EPackage":
              EPackage ePackage = parseEPackage(reader, Collections.EMPTY_MAP);
              ePackage.addPackage(ePackage);
              return Collections.singletonList(ePackage);
            default:
              throw new XMIParseException("Unable to parse root element " + root.getName().toString());
          }
        } else if (event.getEventType() == XMLStreamConstants.COMMENT) {
          logger.info("skipping comment '{}'", event);
          reader.nextEvent();
        } else {
          logger.error("the element is a {}", event.getEventType());
          throw new XMIParseException("Element is not a start element!");
        }

      }
    } catch (XMLStreamException e) {
      throw new XMIParseException("Exception thrown while parsing XML!", e);
    }

    throw new XMIParseException("No contents in XMI file found!");
  }

  private Map<String, String> parseNameSpaces(StartElement element, Map<String, String> previousMap) {
    Iterator namespaceIterator = element.getNamespaces();
    if (!namespaceIterator.hasNext()) {
      return previousMap;
    }
    Map<String, String> result = new HashMap<>(previousMap);
    while (namespaceIterator.hasNext()) {
      Namespace namespace = (Namespace) namespaceIterator.next();
      logger.info("found Namespace {}:{}", namespace.getPrefix(), namespace.getNamespaceURI());
      result.put(namespace.getPrefix(), namespace.getNamespaceURI());
    }
    return result;
  }

  private List<EPackage> parseXMI(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    List<EPackage> contents = new ArrayList<>();


    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      switch (attribute.getName().getLocalPart()) {
        default:
          logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
      }
    }

    // parse the contained elements
    while (reader.hasNext()) {
      XMLEvent nextEvent = reader.peek();
//      logger.debug("looking at ({}:{}) :'{};.", nextEvent.getLocation().getLineNumber(), nextEvent.getLocation().getColumnNumber(), nextEvent);
      if (nextEvent.isStartElement()) {
        if (nextEvent.isStartDocument()) {
          reader.nextEvent();
        } else if (nextEvent.isStartElement()) {
          StartElement root = nextEvent.asStartElement();

          switch (root.getName().getLocalPart()) {
            case "EPackage":
              contents.add(parseEPackage(reader, Collections.EMPTY_MAP));
              break;
            default:
              throw new XMIParseException("Unable to parse root element " + root.getName().toString());
          }
        } else {
          logger.error("the element is a {}", nextEvent.getEventType());
          throw new XMIParseException("Element is not a start element!");
        }

      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();

        for (EPackage ePackage : contents) {
          for (EPackage sibling : contents) {
            ePackage.addPackage(sibling);
          }
        }

        return contents;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event at ({}:{}) {}", nextEvent.getLocation().getLineNumber(), nextEvent.getLocation().getColumnNumber(), nextEvent);
        reader.nextEvent();
      }
    }

    // TODO throw exception

    for (EPackage ePackage : contents) {
      for (EPackage sibling : contents) {
        ePackage.addPackage(sibling);
      }
    }

    return contents;
  }

  private EPackage parseEPackage(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EPackage ePackage = new EPackage();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      switch (attribute.getName().getLocalPart()) {
        case "nsURI":
          ePackage.setNsURI(attribute.getValue());
          break;
        case "nsPrefix":
          ePackage.setNsPrefix(attribute.getValue());
          break;
        case "name":
          ePackage.setName(attribute.getValue());
          break;
        default:
          logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
//      logger.debug("looking at ({}:{}) :'{};.", nextEvent.getLocation().getLineNumber(), nextEvent.getLocation().getColumnNumber(), nextEvent);
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eClassifiers":
            ePackage.addEClassifier(parseEClassifier(reader, nameSpaces));
            break;
          case "eAnnotations":
            ePackage.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          // from EPackage
          case "eSubpackages":
            ePackage.addESubPackage(parseEPackage(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element at ({}:{}) {}:{}", nextEvent.getLocation().getLineNumber(), nextEvent.getLocation().getColumnNumber(), nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return ePackage;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event at ({}:{}) {}", nextEvent.getLocation().getLineNumber(), nextEvent.getLocation().getColumnNumber(), nextEvent);
      }
      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EClassifier parseEClassifier(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {
    StartElement startElement = reader.peek().asStartElement();

    EClassifier eClassifier;

    final String type = startElement.getAttributeByName(XSI_TYPE).getValue();
    switch (type) {
      case "ecore:EClass":
        eClassifier = parseEClass(reader, nameSpaces);
        break;
      case "ecore:EDataType":
        eClassifier = parseEDataType(reader, nameSpaces);
        break;
      case "ecore:EEnum":
        eClassifier = parseEEnum(reader, nameSpaces);
        break;
      default:
        logger.error("Unknown subclass '{}' of EClassifier found", type);
        throw new XMIParseException("Unknown subclass '" + type + "' of EClassifier found");
    }

    return eClassifier;

  }

  private EClassifier parseEClass(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EClass eClass = new EClass();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EClass")) {
          throw new XMIParseException("Expected ecore:EClass but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eClass.setName(attribute.getValue());
            break;
          // from EClassifier
          case "instanceClassName":
            eClass.setInstanceClassName(attribute.getValue());
            break;
          case "instanceClass":
            logger.warn("ignoring transient attribute 'instanceClass' with value '{}'", attribute.getValue());
            break;
          case "defaultValue":
            logger.warn("ignoring transient attribute 'defaultValue' with value '{}'", attribute.getValue());
            break;
          case "ePackage":
            logger.warn("ignoring parent relation 'ePackage' with value '{}'", attribute.getValue());
            break;
          // from EClass
          case "abstract":
            eClass.setAbstract(Boolean.valueOf(attribute.getValue()));
            break;
          case "interface":
            eClass.setInterface(Boolean.valueOf(attribute.getValue()));
            break;
          case "eSuperTypes":
            for (String superType : attribute.getValue().split(" ")) {
              eClass.addESuperType(EClass.createRefDirection(superType));
            }
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eTypeParameters":
            eClass.addETypeParameter(parseETypeParameter(reader, nameSpaces));
            break;
          case "eStructuralFeatures":
            eClass.addEStructuralFeature(parseEStructuralFeature(reader, nameSpaces));
            break;
          case "eOperations":
            eClass.addEOperation(parseEOperation(reader, nameSpaces));
            break;
          case "eGenericSuperTypes":
            eClass.addEGenericSuperType(parseEGenericSuperType(reader, nameSpaces));
            break;
          case "eAnnotations":
            eClass.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eClass;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("in start element {}: ignoring event {}: '{}'", startElement.getName(), nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EAnnotation parseEAnnotation(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EAnnotation eAnnotation = new EAnnotation();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EAnnotation")) {
          throw new XMIParseException("Expected ecore:EAnnotation but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from EAnnotation
          case "source":
            eAnnotation.setSource(attribute.getValue());
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "details":
            eAnnotation.addDetail(parseEStringToMapEntry(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eAnnotation;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EClassifier parseEDataType(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EDataType eDataType = new EDataType();

    // set default values
    eDataType.setSerializable(true);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EDataType")) {
          throw new XMIParseException("Expected ecore:EDataType but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eDataType.setName(attribute.getValue());
            break;
          // from EClassifier
          case "instanceClassName":
            eDataType.setInstanceClassName(attribute.getValue());
            break;
          case "instanceClass":
            logger.warn("ignoring transient attribute 'instanceClass' with value '{}'", attribute.getValue());
            break;
          case "defaultValue":
            logger.warn("ignoring transient attribute 'defaultValue' with value '{}'", attribute.getValue());
            break;
          case "ePackage":
            logger.warn("ignoring parent relation 'ePackage' with value '{}'", attribute.getValue());
            break;
          // from EDataType
          case "serializable":
            eDataType.setSerializable(Boolean.valueOf(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eTypeParameters":
            eDataType.addETypeParameter(parseETypeParameter(reader, nameSpaces));
            break;
          case "eAnnotations":
            eDataType.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eDataType;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("in start element {}: ignoring event {}: '{}'", startElement.getName(), nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EEnum parseEEnum(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EEnum eEnum = new EEnum();

    // set default values
    eEnum.setSerializable(true);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EEnum")) {
          throw new XMIParseException("Expected ecore:EEnum but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eEnum.setName(attribute.getValue());
            break;
          // from EClassifier
          case "instanceClassName":
            eEnum.setInstanceClassName(attribute.getValue());
            break;
          case "instanceClass":
            logger.warn("ignoring transient attribute 'instanceClass' with value '{}'", attribute.getValue());
            break;
          case "defaultValue":
            logger.warn("ignoring transient attribute 'defaultValue' with value '{}'", attribute.getValue());
            break;
          case "ePackage":
            logger.warn("ignoring parent relation 'ePackage' with value '{}'", attribute.getValue());
            break;
          // from EDataType
          case "serializable":
            eEnum.setSerializable(Boolean.valueOf(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eTypeParameters":
            eEnum.addETypeParameter(parseETypeParameter(reader, nameSpaces));
            break;
          case "eAnnotations":
            eEnum.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          case "eLiterals":
            eEnum.addELiteral(parseEEnumLiteral(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eEnum;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("in start element {}: ignoring event {}: '{}'", startElement.getName(), nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EEnumLiteral parseEEnumLiteral(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EEnumLiteral eEnumLiteral = new EEnumLiteral();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EEnumLiteral")) {
          throw new XMIParseException("Expected ecore:EEnumLiteral but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eEnumLiteral.setName(attribute.getValue());
            break;
          // from EEnumLiteral
          case "value":
            eEnumLiteral.setValue(Integer.valueOf(attribute.getValue()));
            break;
          case "literal":
            eEnumLiteral.setLiteral(attribute.getValue());
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eAnnotations":
            eEnumLiteral.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eEnumLiteral;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("in start element {}: ignoring event {}: '{}'", startElement.getName(), nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private ETypeParameter parseETypeParameter(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    ETypeParameter eTypeParameter = new ETypeParameter();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:ETypeParameter")) {
          throw new XMIParseException("Expected ecore:ETypeParameter but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eTypeParameter.setName(attribute.getValue());
            break;
          // from ETypeParameter
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eBounds":
            eTypeParameter.addEBound(parseEGenericType(reader, nameSpaces));
            break;
          case "eAnnotations":
            eTypeParameter.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eTypeParameter;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EParameter parseEParameter(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EParameter eParameter = new EParameter();

    // set default values
    eParameter.setOrdered(true);
    eParameter.setUnique(true);
    eParameter.setUpperBound(1);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EParameter")) {
          throw new XMIParseException("Expected ecore:EParameter but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eParameter.setName(attribute.getValue());
            break;
          // from ETypedElement
          case "ordered":
            eParameter.setOrdered(Boolean.valueOf(attribute.getValue()));
            break;
          case "unique":
            eParameter.setUnique(Boolean.valueOf(attribute.getValue()));
            break;
          case "lowerBound":
            eParameter.setLowerBound(Integer.valueOf(attribute.getValue()));
            break;
          case "upperBound":
            eParameter.setUpperBound(Integer.valueOf(attribute.getValue()));
            break;
          case "eType":
            eParameter.setEType(EClassifier.createRefDirection(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eGenericType":
            eParameter.setEGenericType(parseEGenericType(reader, nameSpaces));
            break;
          case "eAnnotations":
            eParameter.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eParameter;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EGenericType parseEGenericType(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EGenericType eGenericType = new EGenericType();

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EGenericType")) {
          throw new XMIParseException("Expected ecore:EGenericType but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // relations
          case "eClassifier":
            eGenericType.setEClassifier(EClassifier.createRefDirection(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eUpperBound":
            eGenericType.setEUpperBound(parseEGenericType(reader, nameSpaces));
            break;
          case "eLowerBound":
            eGenericType.setELowerBound(parseEGenericType(reader, nameSpaces));
            break;
          case "eTypeArguments":
            eGenericType.addETypeArgument(parseEGenericType(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eGenericType;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EStructuralFeature parseEStructuralFeature(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {
    StartElement startElement = reader.peek().asStartElement();

    EStructuralFeature eStructuralFeature;

    final String type = startElement.getAttributeByName(XSI_TYPE).getValue();
    switch (type) {
      case "ecore:EAttribute":
        eStructuralFeature = parseEAttribute(reader, nameSpaces);
        break;
      case "ecore:EReference":
        eStructuralFeature = parseEReference(reader, nameSpaces);
        break;
      default:
        logger.error("Unknown subclass '{}' of EStructuralFeature found", type);
        throw new XMIParseException("Unknown subclass '" + type + "' of EClassifier found");
    }

    return eStructuralFeature;
  }

  private EOperation parseEOperation(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EOperation eOperation = new EOperation();

    // set default values
    eOperation.setOrdered(true);
    eOperation.setUnique(true);
    eOperation.setUpperBound(1);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EOperation")) {
          throw new XMIParseException("Expected ecore:EOperation but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eOperation.setName(attribute.getValue());
            break;
          // from ETypedElement
          case "ordered":
            eOperation.setOrdered(Boolean.valueOf(attribute.getValue()));
            break;
          case "unique":
            eOperation.setUnique(Boolean.valueOf(attribute.getValue()));
            break;
          case "lowerBound":
            eOperation.setLowerBound(Integer.valueOf(attribute.getValue()));
            break;
          case "upperBound":
            eOperation.setUpperBound(Integer.valueOf(attribute.getValue()));
            break;
          case "eType":
            eOperation.setEType(EClassifier.createRefDirection(attribute.getValue()));
            break;
          // from EOperation
          case "eExceptions":
            for (String eException : attribute.getValue().split(" ")) {
              eOperation.addEException(EClassifier.createRefDirection(eException));
            }
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eGenericType":
            eOperation.setEGenericType(parseEGenericType(reader, nameSpaces));
            logger.info("setting EGenericType in EOperation {} to {}", eOperation.getName(), eOperation.getEGenericType());
            break;
          case "eGenericExceptions":
            eOperation.addEGenericException(parseEGenericType(reader, nameSpaces));
            break;
          case "eTypeParameters":
            eOperation.addETypeParameter(parseETypeParameter(reader, nameSpaces));
            break;
          case "eParameters":
            eOperation.addEParameter(parseEParameter(reader, nameSpaces));
            break;
          case "eAnnotations":
            eOperation.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eOperation;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EGenericType parseEGenericSuperType(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {
    // TODO implement
    throw new RuntimeException("This feature is not implemented yet!");
  }

  private EAttribute parseEAttribute(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EAttribute eAttribute = new EAttribute();

    // set default values
    eAttribute.setOrdered(true);
    eAttribute.setUnique(true);
    eAttribute.setUpperBound(1);
    eAttribute.setChangeable(true);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EAttribute")) {
          throw new XMIParseException("Expected ecore:EAttribute but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {
        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eAttribute.setName(attribute.getValue());
            break;
          // from ETypedElement
          case "ordered":
            eAttribute.setOrdered(Boolean.valueOf(attribute.getValue()));
            break;
          case "unique":
            eAttribute.setUnique(Boolean.valueOf(attribute.getValue()));
            break;
          case "lowerBound":
            eAttribute.setLowerBound(Integer.valueOf(attribute.getValue()));
            break;
          case "upperBound":
            eAttribute.setUpperBound(Integer.valueOf(attribute.getValue()));
            break;
          case "eType":
            eAttribute.setEType(EClassifier.createRefDirection(attribute.getValue()));
            break;
          // from EStructuralFeature
          case "changeable":
            eAttribute.setChangeable(Boolean.valueOf(attribute.getValue()));
            break;
          case "volatile":
            eAttribute.setVolatile(Boolean.valueOf(attribute.getValue()));
            break;
          case "transient":
            eAttribute.setTransient(Boolean.valueOf(attribute.getValue()));
            break;
          case "defaultValueLiteral":
            eAttribute.setDefaultValueLiteral(attribute.getValue());
            break;
          case "defaultValue":
            logger.warn("ignoring attribute 'defaultValue' with value '{}'", attribute.getValue());
            break;
          case "unsettable":
            eAttribute.setUnsettable(Boolean.valueOf(attribute.getValue()));
            break;
          case "derived":
            eAttribute.setDerived(Boolean.valueOf(attribute.getValue()));
            break;
          case "eContainingClass":
            logger.warn("ignoring parent relation 'eContainingClass' with value '{}'", attribute.getValue());
            break;
          // from EAttribute
          case "iD":
            eAttribute.setID(Boolean.valueOf(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }

    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eGenericType":
            eAttribute.setEGenericType(parseEGenericType(reader, nameSpaces));
            break;
          case "eAnnotations":
            eAttribute.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eAttribute;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  private EReference parseEReference(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {

    EReference eReference = new EReference();

    // set default values
    eReference.setOrdered(true);
    eReference.setUnique(true);
    eReference.setUpperBound(1);
    eReference.setChangeable(true);
    eReference.setResolveProxies(true);

    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EReference")) {
          throw new XMIParseException("Expected ecore:EReference but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {

        switch (attribute.getName().getLocalPart()) {
          // from ENamedElement
          case "name":
            eReference.setName(attribute.getValue());
            break;
          // from ETypedElement
          case "ordered":
            eReference.setOrdered(Boolean.valueOf(attribute.getValue()));
            break;
          case "unique":
            eReference.setUnique(Boolean.valueOf(attribute.getValue()));
            break;
          case "lowerBound":
            eReference.setLowerBound(Integer.valueOf(attribute.getValue()));
            break;
          case "upperBound":
            eReference.setUpperBound(Integer.valueOf(attribute.getValue()));
            break;
          case "eType":
            eReference.setEType(EClassifier.createRefDirection(attribute.getValue()));
            break;
          // from EStructuralFeature
          case "changeable":
            eReference.setChangeable(Boolean.valueOf(attribute.getValue()));
            break;
          case "volatile":
            eReference.setVolatile(Boolean.valueOf(attribute.getValue()));
            break;
          case "transient":
            eReference.setTransient(Boolean.valueOf(attribute.getValue()));
            break;
          case "defaultValueLiteral":
            eReference.setDefaultValueLiteral(attribute.getValue());
            break;
          case "unsettable":
            eReference.setUnsettable(Boolean.valueOf(attribute.getValue()));
            break;
          case "derived":
            eReference.setDerived(Boolean.valueOf(attribute.getValue()));
            break;
          case "eContainingClass":
            logger.warn("ignoring parent relation 'eContainingClass' with value '{}'", attribute.getValue());
            break;
          // from EReference
          case "containment":
            eReference.setContainment(Boolean.valueOf(attribute.getValue()));
            break;
          case "resolveProxies":
            eReference.setResolveProxies(Boolean.valueOf(attribute.getValue()));
            break;
          case "eOpposite":
            eReference.setEOpposite(EReference.createRefDirection(attribute.getValue()));
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isStartElement()) {
        final StartElement nextElement = nextEvent.asStartElement();
        switch (nextElement.getName().getLocalPart()) {
          case "eGenericType":
            eReference.setEGenericType(parseEGenericType(reader, nameSpaces));
            break;
          case "eAnnotations":
            eReference.addEAnnotation(parseEAnnotation(reader, nameSpaces));
            break;
          default:
            logger.warn("ignoring element {}:{}", nextElement.getName().getPrefix(), nextElement.getName().getLocalPart());
        }
      } else if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eReference;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }

  // special methods

  private EStringToStringMapEntry parseEStringToMapEntry(final XMLEventReader reader, Map<String, String> nameSpaces) throws XMLStreamException {
    EStringToStringMapEntry eStringToStringMapEntry = new EStringToStringMapEntry();


    // get all the properties out of the element
    StartElement startElement = reader.nextEvent().asStartElement();

    nameSpaces = parseNameSpaces(startElement, nameSpaces);

    for (Iterator attributeIterator = startElement.getAttributes(); attributeIterator.hasNext(); ) {
      Attribute attribute = (Attribute) attributeIterator.next();

      if (attribute.getName().equals(XSI_TYPE)) {
        if (!attribute.getValue().equals("ecore:EStringToStringMapEntry")) {
          throw new XMIParseException("Expected ecore:EReference but found " + attribute.getValue(), attribute.getLocation());
        }
      } else {

        switch (attribute.getName().getLocalPart()) {
          // from EStringToStringMapEntry
          case "key":
            eStringToStringMapEntry.setKey(attribute.getValue());
            break;
          // from EStringToStringMapEntry
          case "value":
            eStringToStringMapEntry.setValue(attribute.getValue());
            break;
          default:
            logger.warn("ignoring attribute {}:{}", attribute.getName().getPrefix(), attribute.getName().getLocalPart());
        }
      }
    }

    // parse the contained elements
    XMLEvent nextEvent = reader.peek();
    while (true) {
      if (nextEvent.isEndElement() && nextEvent.asEndElement().getName().equals(startElement.getName())) {
        reader.nextEvent();
        return eStringToStringMapEntry;
      } else {
        // ignore all other events
        if (nextEvent.getEventType() != XMLStreamConstants.CHARACTERS)
          logger.warn("ignoring event {}: '{}'", nextEvent.getEventType(), nextEvent.toString().replace("\n", "\\n"));
      }

      reader.nextEvent();
      nextEvent = reader.peek();
    }
  }
}
