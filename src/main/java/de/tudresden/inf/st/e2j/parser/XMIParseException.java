package de.tudresden.inf.st.e2j.parser;

import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;

public class XMIParseException extends XMLStreamException {
  public XMIParseException() {
  }

  public XMIParseException(String msg) {
    super(msg);
  }

  public XMIParseException(Throwable th) {
    super(th);
  }

  public XMIParseException(String msg, Throwable th) {
    super(msg, th);
  }

  public XMIParseException(String msg, Location location, Throwable th) {
    super(msg, location, th);
  }

  public XMIParseException(String msg, Location location) {
    super(msg, location);
  }
}
