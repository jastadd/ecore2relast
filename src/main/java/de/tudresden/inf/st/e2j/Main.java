package de.tudresden.inf.st.e2j;

import de.tudresden.inf.st.e2j.jastadd.model.*;
import de.tudresden.inf.st.e2j.parser.EcoreParser;
import de.tudresden.inf.st.e2j.parser.XMIParseException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

public class Main {

  private static final String VERSION = "0.1";

  public static void main(String[] args) {

    if (args.length != 2) {
      printUsage();
    } else {
      transformFile(args[0], args[1]);
    }

  }

  public static void transformFile(String sourceFileName, String targetFileName) {

    if (targetFileName.endsWith(".relast")) {
      targetFileName = targetFileName.substring(0, targetFileName.length() - 7);
    }

    System.out.println(targetFileName);

    String grammarFileName = targetFileName + ".relast";
    String aspectFileName = targetFileName + ".ecore.jadd";

    List<EObject> ePackages = null;

    EcoreParser parser = new EcoreParser();

    // check if the input file exists
    Path sourcePath = Paths.get(sourceFileName);
    if (Files.notExists(sourcePath)) {
      exitWithError("the source file does not exist.");
    }
    Path targetPath = Paths.get(targetFileName);
    if (Files.exists(targetPath)) {
      exitWithError("the target file already exists.");
    }

    try (InputStream stream = Files.newInputStream(sourcePath)) {
      ePackages = parser.parse(stream);
    } catch (IOException e) {
      exitWithError("Unable to open or read source file.");
    } catch(XMIParseException e) {
      exitWithError("Parse error occurred: " + e.getMessage());
    }

    if (ePackages == null || ePackages.size() == 0) {
      exitWithError("No ecore packages were parsed.");
    }

    StringBuilder grammarBuilder = new StringBuilder();
    StringBuilder aspectBuilder = new StringBuilder();

    // print beginning of aspect
    aspectBuilder.append("aspect DataTypes {");

    for (EObject eObject : ePackages) {
      if (!(eObject instanceof EPackage)) {
        exitWithError("One of the parsed top-level objects is no EPackage.");
      }

      EPackage ePackage = ((EPackage) eObject);

      try {
        Grammar grammar = ePackage.getGrammar();
        grammar.print(grammarBuilder);
      } catch (Exception e) {
        exitWithError("Unable to transform Ecore package " + ePackage.getName() + " to grammar.");
      }

      Collection<EEnum> enums = ePackage.enums();
      aspectBuilder.append("// data types\n");
      for (EEnum e : enums) {
        e.printJavaEnum(aspectBuilder);
        aspectBuilder.append("\n");
      }

      grammarBuilder.append("\n\n");
    }


    try (PrintWriter out = new PrintWriter(grammarFileName, StandardCharsets.UTF_8.name())) {
      out.print(grammarBuilder.toString());
    } catch (FileNotFoundException | UnsupportedEncodingException e) {
      exitWithError("Unable to write relast file");
    }

    // print end of aspect
    aspectBuilder.append("}");

    try (PrintWriter out = new PrintWriter(aspectFileName, StandardCharsets.UTF_8.name())) {
      out.print(aspectBuilder.toString());
    } catch (FileNotFoundException | UnsupportedEncodingException e) {
      exitWithError("Unable to write aspect file");
    }
  }

  private static void exitWithError(String message) {
    System.err.println("ERROR: " + message);
    System.err.println("exiting...");
    System.exit(-1);
  }

  private static void printUsage() {
    System.out.println("Ecore2Relast version " + VERSION + ": generate a relational JastAdd grammar from an ecore model");
    System.out.println("Attention: this is a very early version that might contain bugs. please review the generated grammars.");
    System.out.println("Limitations: Only essential structural parts of ecore models are transformed. Multiple inheritance is not supported.");
    System.out.println("Usage:");
    System.out.println("  -h, --help                    print this message");
    System.out.println("  <input.ecore> <output.relast> transform the input ecore model into an equivalent grammar with the given name");
  }

}
