aspect EcoreAPI {


// EClassifier : ENamedElement ::= <InstanceClass:Class> <DefaultValue:Object>;
// EEnumLiteral : ENamedElement ::= <Instance:Enumerator>;
// EReference : EStructuralFeature ::= <Container:boolean>;
// EStructuralFeature : ETypedElement ::= <DefaultValue:Object> ;
// abstract ETypedElement : ENamedElement ::= <Many:boolean> <Required:boolean>;

  /**
   * abstract ETypedElement : ENamedElement ::= <Many:boolean>;
   */
  syn boolean ETypedElement.many() = getUpperBound() < 0 || getUpperBound() > 1;

  /**
   * abstract ETypedElement : ENamedElement ::= <Required:boolean>;
   */
  syn boolean ETypedElement.required() = getLowerBound() > 0;

  /**
   * rel EAttribute.EAttributeType -> EDataType;
   */
  syn EDataType EAttribute.eAttributeType() = getEType().asEDataType();



// rel EClass.EAllAttributes* -> EAttribute;
// rel EClass.EAllReferences* -> EReference;

  /**
   * rel EClass.EReferences* -> EReference;
   */
  syn java.util.List<EReference> EClass.eReferences() {
    java.util.List<EReference> result = new java.util.ArrayList<>();
    for (EStructuralFeature feature : getEStructuralFeatures()) {
      if (feature.isEReference()) {
        result.add(feature.asEReference());
      }
    }
    return result;
  }

  /**
   * rel EClass.EAttributes* -> EAttribute;
   */
  syn java.util.List<EAttribute> EClass.eAttributes() {
    java.util.List<EAttribute> result = new java.util.ArrayList<>();
    for (EStructuralFeature feature : getEStructuralFeatures()) {
      if (feature.isEAttribute()) {
        result.add(feature.asEAttribute());
      }
    }
    return result;
  }

  /**
   * rel EClass.EAllSuperTypes* -> EClass;
   */
  syn java.util.List<EClass> EClass.eAllSuperTypes() {
    java.util.List<EClass> result = new java.util.ArrayList<>();
    for (EClass superType : getESuperTypeList()) {
      result.add(superType);
      result.addAll(superType.eAllSuperTypes());
    }
    return result;
  }

// rel EClass.EAttributes* -> EAttribute;
// rel EClass.EAllStructuralFeatures* -> EStructuralFeature;
// rel EClass.EAllContainments* -> EReference;
//
// rel EClass.EAllOperations* -> EOperation;
// rel EClass.EAllGenericSuperTypes* -> EGenericType;
// rel EClass.EIDAttribute -> EAttribute;

// rel EReference.EReferenceType -> EClass;

// rel EFactory.EPackage <-> EPackage.EFactoryInstance;

// rel EGenericType.ERawType -> EClassifier;

  // inheritance helper attributes

  syn boolean EStructuralFeature.isEReference() = false;
  eq EReference.isEReference() = true;

  syn EReference EStructuralFeature.asEReference() = null;
  eq EReference.asEReference() = this;

  syn boolean EStructuralFeature.isEAttribute() = false;
  eq EAttribute.isEAttribute() = true;

  syn EAttribute EStructuralFeature.asEAttribute() = null;
  eq EAttribute.asEAttribute() = this;

  syn boolean EClassifier.isEDataType() = false;
  eq EDataType.isEDataType() = true;

  syn EDataType EClassifier.asEDataType() = null;
  eq EDataType.asEDataType() = this;

  // containment navigation

  inh EClass EStructuralFeature.eContainingClass();
  eq EClass.getEStructuralFeature().eContainingClass() = this;

}
